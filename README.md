# Xeneta Rates #

This repository provides basic implementation with [Django Framework](https://www.djangoproject.com/) for Http-based API for [Xeneta Rates Task](https://github.com/xeneta/ratestask).

# Initial setup

Project already as Docker setup. You can start project with [initial data](https://github.com/xeneta/ratestask/blob/master/rates.sql) simply by executing following commands within the project root;

```bash
docker-compose build
```
After services are built, run following command;
```bash
docker-compose up -d
```

This process will create postgresql and application with using initial data provided by Xeneta. Application will run at 8000 port.
Since application does not need any frontend access, serving static files is not implemented.

    curl "http://127.0.0.1:8000/rates/?date_from=2016-01-01&date_to=2016-01-10&origin=CNSGH&destination=north_europe_main"
    [
        {
            "day": "2016-01-01",
            "average_price": 1076
        },
        {
            "day": "2016-01-02",
            "average_price": 1076
        },
        {
            "day": "2016-01-03",
            "average_price": 1076
        },
    ]
## For Development
To be able to start the installation;
- install [python3.8](https://www.python.org/downloads/release/python-3810/) to your computer.
- Create a Python 3.8 [virtualenv](https://docs.python.org/3/library/venv.html#creating-virtual-environments)

Once you’ve created a virtual environment, you should [activate](https://docs.python.org/3/tutorial/venv.html#creating-virtual-environments) it.

On Windows, run:
```bash
path\to\venv\Scripts\activate.bat
```

On Unix or MacOS, run:
```bash
source path/to/venv/bin/activate
```

After activating the environment, you should install required packages into the virtual environment. You can do it by using [pip](https://pip.pypa.io/en/stable/user_guide/). Navigate through the project directory and run;
```bash
pip install -r requirements.txt
```
## Running Tests
Before running tests you should change database host settings in *settings.py*;
```
'HOST': 'db', -> 'HOST': 'localhost',
```
Then you can run unit tests if you already activated the environment by;
```bash
python manage.py test 
```