#!/bin/bash
echo 'entrypoint sh is started.'
if [ "$DATABASE" = "postgres" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi
cd xenetarates
python manage.py makemigrations
python manage.py migrate rates 0001_initial --fake
python manage.py migrate
mkdir static
python manage.py collectstatic --noinput

exec "$@"
