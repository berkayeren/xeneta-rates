FROM python:3.8.8
ENV PYTHONUNBUFFERED 1
COPY rates.sql /docker-entrypoint-initdb.d/
RUN mkdir /xeneta_rates
WORKDIR /xeneta_rates
COPY requirements.txt /xeneta_rates/
RUN pip install --upgrade pip
RUN pip install -r requirements.txt
ADD . /xeneta_rates/
COPY . /xeneta_rates/
COPY entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
EXPOSE 8000 5432
ENTRYPOINT ["/entrypoint.sh"]