from django.db import models


class Ports(models.Model):
    code = models.TextField(primary_key=True, max_length=5)
    name = models.TextField()
    parent_slug = models.ForeignKey('Regions', models.DO_NOTHING, db_column='parent_slug')

    class Meta:
        # No database table creation, modification, or deletion operations will be performed for this model.
        managed = False
        db_table = 'ports'


class Prices(models.Model):
    # id field will not be merged to db. This is for django requires at least one primary key attribute per model.
    id = models.AutoField(primary_key=True)
    orig_code = models.ForeignKey(Ports, models.DO_NOTHING, db_column='orig_code',
                                  related_name='orig_code')
    dest_code = models.ForeignKey(Ports, models.DO_NOTHING, db_column='dest_code', related_name='dest_code')
    day = models.DateField()
    price = models.IntegerField()

    class Meta:
        # No database table creation, modification, or deletion operations will be performed for this model.
        managed = False
        db_table = 'prices'


class Regions(models.Model):
    slug = models.TextField(primary_key=True)
    name = models.TextField()
    parent_slug = models.ForeignKey('self', models.DO_NOTHING, db_column='parent_slug', blank=True, null=True)

    class Meta:
        # No database table creation, modification, or deletion operations will be performed for this model.
        managed = False
        db_table = 'regions'
