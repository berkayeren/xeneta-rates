from django.urls import path
from . import views

urlpatterns = [
    path('', views.GetRates.as_view())
]
