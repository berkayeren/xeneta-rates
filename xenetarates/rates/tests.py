from django.test import TestCase
from rest_framework import serializers
from rest_framework.exceptions import ValidationError
from rest_framework.test import APIRequestFactory

from .models import Regions, Ports, Prices
from .serializers import AvgPriceInputSerializer
from .views import GetRates


class GetRatesTestCase(TestCase):
    def setUp(self) -> None:
        Regions.objects.bulk_create([
            Regions(slug='china_main', name='China Main'),
            Regions(slug='northern_europe', name='Northern Europe'),
            Regions(slug='scandinavia', name='Scandinavia', parent_slug_id='northern_europe'),
            Regions(slug='stockholm_area', name='Stockholm Area', parent_slug_id='scandinavia'),
            Regions(slug='uk_sub', name='UK Sub', parent_slug_id='north_europe_sub'),
            Regions(slug='finland_main', name='Finland Main', parent_slug_id='baltic'),
            Regions(slug='baltic_main', name='Baltic Main', parent_slug_id='baltic'),
            Regions(slug='poland_main', name='Poland Main', parent_slug_id='baltic'),
            Regions(slug='kattegat', name='Kattegat', parent_slug_id='scandinavia'),
            Regions(slug='norway_north_west', name='Norway North West', parent_slug_id='scandinavia'),
            Regions(slug='norway_south_east', name='Norway South East', parent_slug_id='scandinavia'),
            Regions(slug='norway_south_west', name='Norway South West', parent_slug_id='scandinavia'),
            Regions(slug='uk_main', name='UK Main', parent_slug_id='north_europe_main'),
            Regions(slug='russia_north_west', name='Russia North West', parent_slug_id='northern_europe'),
            Regions(slug='north_europe_main', name='North Europe Main', parent_slug_id='northern_europe'),
            Regions(slug='north_europe_sub', name='North Europe Sub', parent_slug_id='northern_europe'),
            Regions(slug='china_east_main', name='China East Main', parent_slug_id='china_main'),
            Regions(slug='china_south_main', name='China South Main', parent_slug_id='china_main'),
            Regions(slug='baltic', name='Baltic', parent_slug_id='northern_europe'),
            Regions(slug='china_north_main', name='China North Main', parent_slug_id='china_main'),
        ])
        Ports.objects.bulk_create([
            Ports(code='NLRTM', name='Rotterdam', parent_slug_id='north_europe_main'),
            Ports(code='BEZEE', name='Zeebrugge', parent_slug_id='north_europe_main'),
            Ports(code='FRLEH', name='Le Havre', parent_slug_id='north_europe_main'),
            Ports(code='DEBRV', name='Bremerhaven', parent_slug_id='north_europe_main'),
            Ports(code='BEANR', name='Antwerpen', parent_slug_id='north_europe_main'),
            Ports(code='DEHAM', name='Hamburg', parent_slug_id='north_europe_main'),

        ])
        Prices.objects.bulk_create(
            [
                Prices(orig_code_id='NLRTM', dest_code_id='DEBRV', day='2016-01-01', price=1),
                Prices(orig_code_id='NLRTM', dest_code_id='DEBRV', day='2016-01-01', price=2),
                Prices(orig_code_id='NLRTM', dest_code_id='DEBRV', day='2016-01-01', price=3),
                Prices(orig_code_id='BEZEE', dest_code_id='BEANR', day='2016-01-02', price=1),
                Prices(orig_code_id='BEZEE', dest_code_id='BEANR', day='2016-01-02', price=2),
                Prices(orig_code_id='BEZEE', dest_code_id='BEANR', day='2016-01-02', price=3),
                Prices(orig_code_id='FRLEH', dest_code_id='DEHAM', day='2016-01-03', price=1),
                Prices(orig_code_id='FRLEH', dest_code_id='DEHAM', day='2016-01-03', price=2),
            ]
        )

    def test_get_avg_rates(self):
        factory = APIRequestFactory()
        request = factory.get('/rates/')
        response = GetRates.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]['average_price'], 2)
        self.assertEqual(response.data[1]['average_price'], 2)
        self.assertEqual(response.data[2]['average_price'], None)

    def test_get_avg_rates_query_params(self):
        factory = APIRequestFactory()
        request = factory.get('/rates/', {
            'date_from': '2016-01-01',
            'date_to': '2016-01-02',
            'origin': 'north_europe_main',
            'destination': 'DEBRV'
        })
        response = GetRates.as_view()(request)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.data[0]['average_price'], 2)
        self.assertEqual(len(response.data), 1)

    def test_from_greater_than_to(self):
        factory = APIRequestFactory()
        request = factory.get('/rates/', {
            'date_from': '2016-01-10',
            'date_to': '2016-01-01'
        })
        response = GetRates.as_view()(request)
        self.assertEqual(response.status_code, 400)
        self.assertIn('datefromto', response.data)
