from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('rates', '0001_initial'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='ports',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='prices',
            options={'managed': False},
        ),
        migrations.AlterModelOptions(
            name='regions',
            options={'managed': False},
        ),
    ]
