from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Ports',
            fields=[
                ('code', models.TextField(max_length=5, primary_key=True, serialize=False)),
                ('name', models.TextField()),
            ],
            options={
                'db_table': 'ports',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Regions',
            fields=[
                ('slug', models.TextField(primary_key=True, serialize=False)),
                ('name', models.TextField()),
                ('parent_slug', models.ForeignKey(blank=True, db_column='parent_slug', null=True, on_delete=django.db.models.deletion.DO_NOTHING, to='rates.regions')),
            ],
            options={
                'db_table': 'regions',
                'managed': True,
            },
        ),
        migrations.CreateModel(
            name='Prices',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False)),
                ('day', models.DateField()),
                ('price', models.IntegerField()),
                ('dest_code', models.ForeignKey(db_column='dest_code', on_delete=django.db.models.deletion.DO_NOTHING, related_name='dest_code', to='rates.ports')),
                ('orig_code', models.ForeignKey(db_column='orig_code', on_delete=django.db.models.deletion.DO_NOTHING, related_name='orig_code', to='rates.ports')),
            ],
            options={
                'db_table': 'prices',
                'managed': True,
            },
        ),
        migrations.AddField(
            model_name='ports',
            name='parent_slug',
            field=models.ForeignKey(db_column='parent_slug', on_delete=django.db.models.deletion.DO_NOTHING, to='rates.regions'),
        ),
    ]
