from django.db.models import Q, Avg, Count, Case, When, ExpressionWrapper, F, IntegerField
from rest_framework import status
from rest_framework.permissions import AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView
from .models import *
from .serializers import AvgPriceSerializer, AvgPriceInputSerializer


class GetRates(APIView):
    permission_classes = (AllowAny,)

    def get(self, request):
        serializer = AvgPriceInputSerializer(data=request.query_params)
        if serializer.is_valid() is not True:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        date_from = serializer.data.get('date_from', None)
        date_to = serializer.data.get('date_to', None)
        origin = serializer.data.get('origin', None)
        destination = serializer.data.get('destination', None)
        query_set = Q()

        if date_from is not None:
            query_set = Q(day__gte=date_from)

        if date_to is not None:
            query_set &= Q(day__lte=date_to)

        if origin is not None:
            query_set &= (Q(orig_code__code=origin) | Q(orig_code__parent_slug__slug=origin))

        if destination is not None:
            query_set &= (Q(dest_code__code=destination) | Q(dest_code__parent_slug__slug=destination))

        # Following orm call will produce this sql query,
        # SELECT "prices"."day",
        #        COUNT("prices"."day")                                                         AS "sale_count",
        #        CASE WHEN COUNT("prices"."day") >= 3 THEN AVG("prices"."price") ELSE NULL END AS "average_price"
        # FROM "prices"
        #          INNER JOIN "ports" ON ("prices"."orig_code" = "ports"."code")
        #          INNER JOIN "ports" T4 ON ("prices"."dest_code" = T4."code")
        # WHERE ("prices"."day" >= date1 AND "prices"."day" <= date2 AND
        #        ("prices"."orig_code" = x OR "ports"."parent_slug" = x) AND
        #        ("prices"."dest_code" = y OR T4."parent_slug" = y))
        # GROUP BY "prices"."day"

        prices = Prices.objects.filter(query_set).values('day').annotate(sale_count=Count('day')).annotate(
            average_price=ExpressionWrapper(Case(
                When(sale_count__gte=3, then=Avg(F('price'), output_field=IntegerField())),
            ),
                output_field=IntegerField()
            ), ).order_by('day')
        return Response(AvgPriceSerializer(prices, many=True).data)
