from rest_framework import serializers
from .models import Prices


class AvgPriceSerializer(serializers.ModelSerializer):
    day = serializers.DateField(read_only=True)
    average_price = serializers.IntegerField(read_only=True)

    class Meta:
        model = Prices
        fields = [
            'day',
            'average_price',
        ]


class AvgPriceInputSerializer(serializers.Serializer):
    date_from = serializers.DateField(required=False)
    date_to = serializers.DateField(required=False)
    origin = serializers.CharField(required=False)
    destination = serializers.CharField(required=False)

    def validate(self, data):
        if data.__contains__('date_from') and data.__contains__('date_to') and data['date_from'] > data['date_to']:
            raise serializers.ValidationError({
                'datefromto': "Date from cannot be greater then date to"
            })
        return super(AvgPriceInputSerializer, self).validate(data)
